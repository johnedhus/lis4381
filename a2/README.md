# LIS 4381

## John Husum

### Assignment 2 Requirements:

1. Create a mobile recipe app using Android Studio.
2. Chapter Questions (Ch 3 & 4)

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshotofrunning application’s firstuser interface
* Screenshotofrunning application’s seconduser interface;

#### Assignment Screenshots:

|Firstuser Interface:|Seconduser Interface:|
|---|---|
|![open](img/open.gif)|![run](img/run.gif)|

#### Skillsets 1-3:

|ss1 code:|ss1 java:|
|---|---|
|![codess1](img/ss1Code.png)|![runss1](img/ss1Test.png)|

|ss2 code:|ss2 java:|
|---|---|
|![codess2](img/ss2Code.PNG)|![runss2](img/ss2Test.png)|

|ss3 code:|ss3 java:|
|---|---|
|![codess3](img/ss3Code.PNG)|![runss3](img/ss3Test.png)|