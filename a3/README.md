# LIS 4381

## John Husum

### Assignment 3 Requirements:

1. Create a Database with 3 tables and present ERD
2. Create a Mobile App that calculates ticket cost
3. Chapters 5 & 6 questions
4. Skillsets 4 to 6

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshot of running ERD;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;
* Screenshots of 10 records for each table

#### Assignment Screenshots:

|ERD:|
|---|
|![erd](img/erd.png)|

|Firstuser Interface:|Seconduser Interface:|
|---|---|
|![first](img/first.png)|![second](img/second.png)|

|10 Records for Each Table:|
|---|
|![record 1](img/r1.png)|
|![record 2](img/r2.png)|
|![record 3](img/r3.PNG)|

#### Links

[a3.mwb](file/a3.mwb)

[a3.sql](file/a3.sql)

#### Skillsets 4-6:

##### Skillset 4
![ss4](img/ss4.gif)
``` java
import java.util.Scanner;

public class DecisionStructures
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("W or w (work), C or c (cell), H or h (home), N or n (none).\nPhone Types: ");
        char Char = scan.next().charAt(0);

        System.out.println("\nif...else:");
        if (Char == 'W' || Char == 'w')
            System.out.print("work");
        else
        if (Char == 'C' || Char == 'c')
            System.out.print("cell");
        else
        if (Char == 'H' || Char == 'h')
            System.out.print("home");
        else
        if (Char == 'N' || Char == 'n')
            System.out.print("none");
        else 
            System.out.print("Incorrect character entry.");
            
        System.out.println("\n\nswitch:");
        switch (Char)
        {
            case 'W': case 'w':
                System.out.print("work");
                break;
            case 'C': case 'c':
                System.out.print("cell");
                break;
            case 'H': case 'h':
                System.out.print("home");
                break;
            case 'N': case 'n':
                System.out.print("none");
                break;
            default:
                System.out.print("Incorrect character entry.");
                break;
        }
    }
}

```

##### Skillset 5 Nested Structures
![ss5 A](img/ss5A.gif)
``` java
import java.util.Scanner;

public class NestedStructures
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int[] intArray = {3, 2, 4, 99, -1, -5, 3, 7};

        System.out.print("Array length: 8\nEnter search valve: ");
        int value = scan.nextInt();

        System.out.println();
        for(int i=0; i<8; i++)
        {
            if (value == intArray[i])
                System.out.println(value + " is found at index " + i);
            else
                System.out.println(value + " is *NOT* found at index " + i);
        }

    }
}

```

##### Skillset 5 Random Array
![ss5 B](img/ss5B.gif)
``` java
import java.util.Scanner;
import java.util.Random;

public class RandomArray
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        int MIN_VALUE = -1073741823,
            MAX_VALUE = 1073741824,
            num;

        System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
        int length = scan.nextInt();
        
        System.out.println("\nfor loop:");
        for(int i=0; i<length; i++)
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
        }

        System.out.println("\nEnhanced for loop:");
        int[] lengthArr = new int[length];
        for(int i=0; i<length; i++)
            lengthArr[i]=i;
        for(int i : lengthArr)
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
        }

        System.out.println("\nwhile loop:");
        int j = 0;
        while(j < length)
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
            j++;
        }

        System.out.println("\ndo...while loop:");
        int k = 0;
        do
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
            k++;
        } while(k < length);
    }
} 

```
##### Skillset 6
![ss5](img/ss6.gif)
``` java
import java.util.Scanner;
import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Program prompts user for first name and age, then prints results.");
    }

    public static void getUserInput()
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter first name: ");
        String name = scan.nextLine();
        System.out.print("Enter age: ");
        int age = scan.nextInt();

        myVoidMethod(name, age);
        System.out.print(myValueReturningMethod(name, age));
    }

    public static void myVoidMethod(String n, int a)
    {
        System.out.print("\nvoid method call: " + n + " is " + a);
    }

    public static String myValueReturningMethod(String n, int a)
    {
        String s = ("\nvalue-returning method call: " + n + " is " + a);
        return s;
    }

    public static void main(String[] args)
    {
        getRequirements();
        getUserInput();  
    }
}

```