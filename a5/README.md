# LIS 4381

## John Husum

<<<<<<< HEAD
### Assignment 4 Requirements:
1. A4 cloned files
=======
### Assignment 5 Requirements:
1. A5 cloned files
>>>>>>> f081d0da01a16bec0f31224c164bf3c706a4ea87
2. Review subdirectories and files
3. Open index.php and review code

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples; 
* Link to local lis4381 web app: http://localhost/repos/lis4381/

#### Assignment Screenshots:

|Before *and* after successful add:|
|---|
|![add](img/working.gif)|

|Failed Validation:|
|---|
|![fail](img/error.gif)|

#### Skillsets 13-15:

##### Skillset 13
![ss13](img/ss13.png)
``` java
import java.util.Scanner;
import java.lang.Math;
import java.text.DecimalFormat;

public class SphereVolumeCalculator
{
    public static void main(String[] args)
    {
        DecimalFormat df = new DecimalFormat("####,####.##");
        Scanner scan = new Scanner(System.in);
        boolean run = true, test = true;
        String input = "";

        while(run)
        {
            while(test)
            {
                System.out.print("Enter diameter: ");
                input = scan.nextLine();
                try
                {
                    Integer.parseInt(input);
                    break;
                }
                catch(NumberFormatException e)
                {
                    System.out.print("Not a valid integer!\n\nPlease try again. ");
                }
            }

            System.out.print("Volume: " + df.format(((4.0/3.0) * Math.PI * Math.pow(Integer.parseInt(input), 3)) / 231) +
                             " US gallons\n\nDo you want to calculate another sphere? (y or n)? ");
            input = scan.nextLine();
            if(input.charAt(0) == 'N' || input.charAt(0) == 'n')
            {
                break;
            }
        }
    }
}

```

##### Skillset 14
![ss14](img/ss14.gif)

##### Skillset 15
![ss15](img/ss15.gif)
