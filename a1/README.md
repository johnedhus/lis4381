# LIS 4381

## John Husum

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch 1 & 2)

#### README.md file should include the following items:

* Screenshot of ampps installation running
* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App 
* git commands w/short descriptions
* Bitbucket repo links

> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository
2. git status - Displays that state of the working directory
3. git add - Updates the index
4. git commit - Records changes to the repository
5. git push - Updates the remote refs using the local refs
6. git pull - Fetches and downloads content from remote repository
7. git clone - Clone a repository
#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Bitbucket links:

*Bitbucket A1:*
[A1 Bitbucket Assignment Link](https://bitbucket.org/johnedhus/lis4381/src/master/a1/ "Bitbucket A1")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/johnedhus/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
