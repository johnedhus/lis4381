# LIS 4381

## John Husum

### Project 2 Requirements:
1. Suitably modify meta tags 
2. Change title, navigation links, and header tags appropriately 
3. See videos for complete development
4. Turn off client-side validation by commenting out the following code
5. Add server-side validation and regular expressions-- as per the database entity attribute requirements 

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples; 
* Link to local lis4381 web app: http://localhost/repos/lis4381/

#### Assignment Screenshots:

|Homepage and Carousel|
|---|
|![carousel](img/home-page.gif)

|Before *and* after successful edit:|Failed Validation:|
|---|---|
|![edit-good](img/edit-good.gif)|![edit-bad](img/edit-bad.gif)|

|Delete prompt *and* successfully deleted record:|RSS feed :|
|---|---|
|![edit-good](img/delete-this.gif)|![edit-bad](img/rss-feed.PNG)|
