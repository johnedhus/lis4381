> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## John Husum

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My Frist App
    - Provide screenshots of installations
    - Created Bitbucket rep
    - Provide git command and descriptions
    - Complete Bitbucke tutorials

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a mobile recipe app using Android Studio
    - Provide screenshots
    - Skillsets 1 to 3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Coursetitle, your name, assignment requirements, as per A1
    - Screenshot of running ERD;
    - Screenshot of running application’s first user interface;
    - Screenshot of running application’s second user interface;
    - Screenshots of 10 records for each table
    - Skillsets 4 to 6

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Web App
    - Added four images to carousel
    - Skillsets 10 to 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create Data Validation Web App
    - Test for valid input
    - Skillsets 13 to 15

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Backward-Engineer provided screenshots and added personal photo, contact information, and intrest
    - Provide screenshots of application
    - Skillsets 7 to 9
	
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Add server-side validation and regular expressions
    - Web app should edit and delete data
    - Test for vaild input