# LIS 4381

## John Husum

### Project 1 Requirements:

1. Create a launcher icon image and display it in both activities 
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)
5. Chapters 7 & 8 questions
6. Skillsets 7 to 9

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;

#### Project Screenshots:

|First User Interface:|Second User Interface:|
|---|---|
|![first](img/first.png)|![second](img/second.gif)|


#### Skillsets 7-9:

##### Skillset 7
![ss7](img/ss7.gif)
``` java
import java.util.Scanner;
import java.util.Random;

public class RanDatArrVal
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        int MIN_VALUE = -1073741823,
            MAX_VALUE = 1073741824,
            num,
            length=0;
        boolean dataValid = false;

        System.out.print("Enter desired number of pseudo-random integers (min 1): ");
        while (dataValid == false)
        {
            try
            {
                length = scan.nextInt();
                if(length < 1)
                {
                    System.out.print("\nNumber must be greater the 0. Please enter integer greater than 0: ");
                    length = scan.nextInt();
                }
                else
                    dataValid = true;
            }
            catch (Exception e)
            {
                scan.nextLine();
                System.out.print("Not valid integer!\n\nPlease Try again. Enter Valid interger (min 1): ");
            }
        }

        System.out.println("\nfor loop:");
        for(int i=0; i<length; i++)
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
        }

        System.out.println("\nEnhanced for loop:");
        int[] lengthArr = new int[length];
        for(int i=0; i<length; i++)
            lengthArr[i]=i;
        for(int i : lengthArr)
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
        }

        System.out.println("\nwhile loop:");
        int j = 0;
        while(j < length)
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
            j++;
        }

        System.out.println("\ndo...while loop:");
        int k = 0;
        do
        {
            num = random.nextInt(MAX_VALUE - MIN_VALUE) + MAX_VALUE;
            System.out.println(num);
            k++;
        } while(k < length);
    }
}

```

##### Skillset 8
![ss8](img/ss8.gif)
``` java
import java.util.Scanner;

public class ThreeNums
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int[] nums = new int[3];
        boolean dataValid = false;

        System.out.print(
            "Program evaluates largest of the three integers.\n" +
            "Note: Program checks for integers and non-numeric values.\n\n" + 
            "Please enter the first number: ");
        while (dataValid == false)
        {
            try
            {
                nums[0] = scan.nextInt();
                dataValid = true;
            }
            catch (Exception e)
            {
                scan.nextLine();
                System.out.print("Not valid integer!\n\nPlease Try again. Enter first number: ");
            }
        }

        dataValid = false;
        System.out.print("\nPlease enter the second number: ");
        while (dataValid == false)
        {
            try
            {
                nums[1] = scan.nextInt();
                dataValid = true;
            }
            catch (Exception e)
            {
                scan.nextLine();
                System.out.print("Not valid integer!\n\nPlease Try again. Enter second number: ");
            }
        }

        dataValid = false;
        System.out.print("\nPlease enter the third number: ");
        while (dataValid == false)
        {
            try
            {
                nums[2] = scan.nextInt();
                dataValid = true;
            }
            catch (Exception e)
            {
                scan.nextLine();
                System.out.print("Not valid integer!\n\nPlease Try again. Enter third number: ");
            }
        }

        if(nums[0] > nums[1] && nums[0] > nums[2])
            System.out.print("\nFirst number is the largest");
        
        else if(nums[1] > nums[0] && nums[1] > nums[2])
            System.out.print("\nSecond number is the largest");
        
        else
            System.out.print("\nThird number is the largest");
    }
}

```

##### Skillset 9
![ss9](img/ss9.gif)
``` java
import java.util.Scanner;

public class ArrRunTim
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int size = 0;
        boolean dataValid = false;
        
        System.out.print("Please enter array size: ");
        while (dataValid == false)
        {
            try
            {
                size = scan.nextInt();
                dataValid = true;
            }
            catch (Exception e)
            {
                scan.nextLine();
                System.out.print("Not valid integer!\n\nPlease Try again. Enter array size: ");
            }
        }

        float[] nums = new float[size];
        for(int i=0; i<size; i++)
        {
            dataValid = false;
            while (dataValid == false)
            {
                try
                {
                    System.out.print("Enter num " + (i+1) + ":  ");
                    nums[i] = scan.nextFloat();
                    dataValid = true;
                }
                catch (Exception e)
                {
                    scan.nextLine();
                    System.out.print("\nNot valid number! ");
                }
            }
        }

        System.out.print("\nNumbers entered: ");
        float sum = 0;
        for(int i=0; i<size; i++)
        {
            System.out.print(nums[i] + " ");
            sum += nums[i];
        }
        System.out.print("\nSum: " + sum + "\nAverage: " + sum/size);
        
    }    
}


```
