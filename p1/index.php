<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="John Husum">
    <link rel="icon" href="../favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> This application is a virtual business card.
				</p>

				<table>
					<tr>
						<td><h4>First User Interface</h4></td>
						<td><h4>Second User Interface</h4></td>
					</tr>
					<tr>
						<td><img src="img/first.png" class="img-responsive center-block" alt="first"></td>
						<td><img src="img/second.gif" class="img-responsive center-block" alt="second"></td>
					</tr>
				</table>

				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    	</div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
