# LIS 4381

## John Husum

### Assignment 4 Requirements:
1. cd to local repos subdirectory:  
2. Clone assignment starter files:  
3. Review subdirectories and files 
4. Open index.php and review code: 
5. Create a favicon using *your* initials, and place it in each assignment’s main directory

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples; 
* Link to local lis4381 web app;

#### Assignment Screenshots:

##### Link to [Webapp](http://localhost/repos/lis4381/)

|Home Page:|
|---|---|
|![home](img/home.gif)|

|Failed Validation:|
|---|---|
|![fail](img/fail.jpg)|

|Failed Validation:|
|---|---|
|![win](img/win.jpg)|

#### Skillsets 10-12:

##### Skillset 10
![ss10](img/ss10.gif)
``` java
import java.util.Scanner;
import java.util.ArrayList;


public class ArrayListDemo
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        ArrayList<String> animals = new ArrayList<String>();
        int k=0;
        boolean run=true;
        char input;
        
        
        while(run)
        {
            System.out.print("Enter animal type: ");
            animals.add(scan.next());
            System.out.print("ArrayList elements: [" + animals.get(0));
            for(int i=1; i<=k; i++)
                System.out.print(", " + animals.get(i));
            System.out.print("]\nArrayList Size = " + (k+1) + "\n\nCountinue? Enter y or n: " );
            input = scan.next().charAt(0);
            if(input == 'y')
                k++;
            else if(input == 'n')
                run = false;
        }
    }    
}


```

##### Skillset 11
![ss11](img/ss11.gif)
``` java
import java.util.Scanner;

public class AlphaNumericSpecial
{   
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter character: ");
        char input = scan.next().charAt(0);
        int ascii = input;

        System.out.print(input + " is ");
        if((ascii >= 65 && ascii <= 90) || (ascii >= 97 && ascii <= 122))
            System.out.print("alpha. ");
        else if(ascii >= 48 && ascii <= 57)
            System.out.print("numeric. ");
        else
            System.out.print("special character. ");
        System.out.print("ASCII value: " + ascii);
    }
}

```

##### Skillset 12
![ss12](img/ss12.gif)
``` java
import java.util.Scanner;

public class TemperatureConversion
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        boolean run = true;
        double value;
        char temp;

        while(run)
        {
            System.out.print("F or C: ");
            temp = scan.next().charAt(0);
            
            if(temp == 'F' || temp == 'f')
            {
                System.out.print("Enter temperature in Fahrenheit: ");
                value = scan.nextDouble();
                System.out.println("Temperature in Celsius is " + ((value-32) * 5 / 9)); 
            }

            else if(temp == 'C' || temp == 'c')
            {
                System.out.print("Enter temperature in Celsius: ");
                value = scan.nextDouble();
                System.out.println("Temperature in Fahrenheit is " + (1.8 * value + 32)); 
            }
            else
                System.out.print("Invalid input.\n\n");

            System.out.print("Do you wish to convert more? (y or n): ");
            temp = scan.next().charAt(0);
            if(temp == 'y' || temp == 'Y')
                System.out.println();
            else if(temp == 'n' || temp == 'N')
                run = false;
        }
    }
}

```
